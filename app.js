var express = require("express");
var app = express();
var fs = require('fs');
var path = require('path');

var multer = require('multer');
var csv = require('fast-csv');
// require csvtojson
var convFile = require("csvtojson");

var upload = multer({dest: 'tmp/csv/'});
app.listen(3000, () => {
 console.log("Server running on port 3000");
});

app.get("/", (req, res, next) => {
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
});

    
app.post('/handleFile',upload.single('uploadCsv'), function(req, res, next) {
    console.log('received')
    var filePath = path.join(__dirname, './' + req.file.path);

    //Use async / await
    async function test(filePath) {
        console.log("Vor der await-Funktion");
        const jsonArray=await convFile().fromFile(filePath);
        console.log("Nach der await Funktion");
        console.log(jsonArray); 
        res.status(200).send({ success: "Success", body: jsonArray });
    }
    
    test(filePath); 
});
